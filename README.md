# README #

### What is this repository for? ###

This repository was created by Gunnar Marteinsson and Sonja Jonsdottir. The reason is to create the and Android App Dots as a School Project.  

### Dots - Project Description ###

Your task is to make an app for playing a game called Dots. To learn about the game,
please download the Dots app (by PlayDots Inc.) from the Google PlayStore
(https://play.google.com/store/apps/details?id=com.nerdyoctopus.gamedots).

### How do I get set up? ###

* Android Studio
* Android Device / Emulator (Genymotion recommmended)
* Build and run from Android Studio

### Contributors ###
Gunnar Marteinsson
Sonja Jonsdottir
