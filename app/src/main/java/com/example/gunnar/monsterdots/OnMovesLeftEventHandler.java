package com.example.gunnar.monsterdots;

/**
 * Created by Gunnar on 19-Sep-15.
 */
public interface OnMovesLeftEventHandler {
    void finalGameScore (int score);
}
