package com.example.gunnar.monsterdots;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by SonjaJons on 08/09/15.
 */
public class DotsPreferences extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
