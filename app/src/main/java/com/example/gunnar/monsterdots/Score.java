package com.example.gunnar.monsterdots;

/**
 * Created by Gunnar on 19-Sep-15.
 */
public class Score implements Comparable<Score> {
    public String _name;
    public int _score;

    public Score(){
    }

    public Score(String name, int record){
        this._name = name;
        this._score = record;
    }

    public int get_score() {
        return _score;
    }

    @Override
    public int compareTo(Score record) {
        int returnValue = ((Score)record)._score;

        /* For Descending order do like this */
        return returnValue-this._score;
    }
}
