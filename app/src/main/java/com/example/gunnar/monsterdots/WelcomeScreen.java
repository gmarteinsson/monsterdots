package com.example.gunnar.monsterdots;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class WelcomeScreen extends Activity {

    DataBaseHandler m_db;
    SharedPreferences m_sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        m_db = new DataBaseHandler(this);

        m_sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        checkTheme();
        setContentView(R.layout.activity_welcome_screen);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_welcome_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,DotsPreferences.class);
            startActivity( intent );
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void menuClick(View view) {
        if(view.getId() == R.id.highScore){
            Intent intent = new Intent(this, HighScore.class);
            startActivity(intent);
        }
        else if(view.getId() == R.id.options){
            Intent intent = new Intent(this, DotsPreferences.class);
            startActivity(intent);
        }
        else if(view.getId() == R.id.play){
            Intent intent = new Intent(this, BoardActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onRestart() {
        m_sp = PreferenceManager.getDefaultSharedPreferences(this);
        checkTheme();
        super.onRestart();
    }

    public void checkTheme() {
        boolean darkMode = m_sp.getBoolean("darkMode", false);
        if (darkMode) {
            setTheme(R.style.dark);
            setContentView(R.layout.activity_welcome_screen);
        } else {
            setTheme(R.style.light);
            setContentView(R.layout.activity_welcome_screen);
        }
    }
}
