package com.example.gunnar.monsterdots;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DataBaseHandler extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "ScoreManager";
    private static final String TABLE_SCORES = "Scores";

    private static final String NAME = "name";
    private static final String SCORE = "score";

    public DataBaseHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_HIGHSCORE_TABLE = "CREATE TABLE " + TABLE_SCORES + "(" +
                NAME + " TEXT, " + SCORE + " INTEGER)";
        db.execSQL(CREATE_HIGHSCORE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCORES);

        // Create tables again
        onCreate(db);
    }

    public void addScore(Score score) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(NAME, score._name);
        values.put(SCORE, score._score);

        db.insert(TABLE_SCORES, null, values);
        db.close();
    }

    public ArrayList<Score> getAllScores() {
        ArrayList<Score> scoreList = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_SCORES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()) {
            do {
                Score score = new Score();
                score._name = cursor.getString(0);
                score._score = cursor.getInt(1);
                scoreList.add(score);
            } while(cursor.moveToNext());
        }
        cursor.close();

        return scoreList;
    }

}