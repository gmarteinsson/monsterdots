package com.example.gunnar.monsterdots;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by SonjaJons on 07/09/15.
 */
public class HighScore extends WelcomeScreen {

    ListView listView;
    ScoreAdapter scoreAdapter;
    ArrayList<Score> scores;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.high_score_screen);

        listView = (ListView) findViewById(R.id.records);

        // ResetHighScore button...
        Button clearHighScore = (Button) findViewById(R.id.resetHighScore);
            clearHighScore.setOnClickListener(
                    new Button.OnClickListener() {
                        public void onClick(View v) {
                            scores.clear();
                            deleteDatabase("ScoreManager");
                            listView.setAdapter(scoreAdapter);
                        }
                    }
            );
    }

    public void onResume() {
        super.onResume();

        scores = m_db.getAllScores();
        Collections.sort(scores);

        scoreAdapter = new ScoreAdapter(this, scores);

        listView.setAdapter(scoreAdapter);
    }
}
