package com.example.gunnar.monsterdots;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by SonjaJons on 08/09/15.
 */
public class BoardActivity extends WelcomeScreen {

    private static boolean sounds;
    static MediaPlayer mpOnClickSound;
    SharedPreferences getPrefs;
    final Context context = this;

    Board boardView;

    private static Vibrator m_vibrator;
    private static Boolean m_use_vibrator = true;

    private static int NUM_CELLS;

    public BoardActivity() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.board_screen);

        boardView = (Board) findViewById(R.id.boardView);
        boardView.setMovesLeftEventHandler(new OnMovesLeftEventHandler() {
            @Override
            public void finalGameScore(int score) {
                moveToHighScore(score);
            }
        });

        getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        m_vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);

        // Get soundOnOff from preference screen.
        sounds = getPrefs.getBoolean("soundCheck", true);

        // Sound can be found under raw folder called button1
        mpOnClickSound = MediaPlayer.create(this, R.raw.button1);

        View fadeView = findViewById(R.id.boardView);
        Animation fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in_anim);
        fadeView.startAnimation(fadeInAnimation);
    }

    static void playSound() {
        if(isSounds()) {
            mpOnClickSound.start();
        }
    }

    // Getter for sound
    public static boolean isSounds() {
        return sounds;
    }

    @Override
    protected void onStart() {
        super.onStart();
        sounds = getPrefs.getBoolean("soundCheck", true);
        m_use_vibrator = getPrefs.getBoolean("vibrate", false);
        NUM_CELLS = Integer.parseInt(getPrefs.getString("gridSize", "6"));
    }

    public static void vibrate() {
        if ( m_use_vibrator ) {
            m_vibrator.vibrate(500);
        }
    }

    void moveToHighScore(final int gameScore) {

        final EditText playerName = new EditText(context);
        playerName.setInputType(InputType.TYPE_CLASS_TEXT);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // DialogBox Title
        alertDialogBuilder.setTitle("Nice Job!" + " " + "Total score: " + gameScore);

        // Dialog message
        alertDialogBuilder
                .setMessage("Store your record!")
                .setCancelable(false)
                .setView(playerName)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        String name = "";
                        name = playerName.getText().toString();
                        Score score = new Score(name, gameScore);
                        m_db.addScore(score);
                        finish();
                        Intent intent = new Intent(getApplicationContext(), HighScore.class);
                        startActivity(intent);
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
}
