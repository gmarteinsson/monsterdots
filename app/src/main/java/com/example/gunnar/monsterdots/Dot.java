package com.example.gunnar.monsterdots;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;

import java.util.Random;

/**
 * Created by SonjaJons on 10/09/15.
 */
public class Dot {

    private int x, y;
    public RectF m_circle;
    public Paint m_paintCircle;
    public int myColor;

    public Dot() {
    }

    public Dot(int x, int y) {
        this.x = x;
        this.y = y;

        m_circle = new RectF();
        m_paintCircle = new Paint();

        randomColor(this);
        m_paintCircle.setColor(this.myColor);
        m_paintCircle.setStyle(Paint.Style.FILL_AND_STROKE);
        m_paintCircle.setAntiAlias(true);
    }

    public void changeColor(int color) {
        myColor = color;
        m_paintCircle.setColor(this.myColor);
    }

    public static int newRandomColor() {
        Random rand = new Random();
        int randomNum = rand.nextInt(5);
        switch (randomNum) {
            case 0: // Blue
                return Color.parseColor("#33CCFF");
            case 1: // Pink
                return Color.parseColor("#FF99CC");
            case 2: // Purple
                return Color.parseColor("#B178CF");
            case 3: // Orange
                return Color.parseColor("#FFB870");
            case 4: // Green
                return Color.parseColor("#45E6A6");
            default:
                return Color.WHITE;
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public static void randomColor(Dot dot) {
        Random rand = new Random();
        int randomNum = rand.nextInt(5);
        switch (randomNum) {
            case 0: // Blue
                dot.myColor = Color.parseColor("#33CCFF");
                break;
            case 1: // Pink
                dot.myColor = Color.parseColor("#FF99CC");
                break;
            case 2: // Purple
                dot.myColor = Color.parseColor("#B178CF");
                break;
            case 3: // Orange
                dot.myColor = Color.parseColor("#FFB870");
                break;
            case 4: // Green
                dot.myColor = Color.parseColor("#45E6A6");
                break;
            default:
                dot.myColor = Color.WHITE;
                break;
        }
    }
}
