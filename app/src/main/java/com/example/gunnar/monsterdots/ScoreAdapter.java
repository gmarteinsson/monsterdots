package com.example.gunnar.monsterdots;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ScoreAdapter extends ArrayAdapter<Score> {
    public ScoreAdapter(Context context, ArrayList<Score> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Score hs = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listview_entry, parent, false);
        }

        TextView playerName = (TextView) convertView.findViewById(R.id.playerScore);
        TextView score = (TextView) convertView.findViewById(R.id.recordScore);

        String value = Integer.toString(hs._score);
        playerName.setText("#" + (position + 1) + "  " + hs._name);
        score.setText(value);

        return convertView;
    }
}