package com.example.gunnar.monsterdots;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by SonjaJons on 08/09/15.
 */
public class Board extends View {

    SharedPreferences getPrefs;

    private OnMovesLeftEventHandler movesLeftHandler = null;

    private ArrayList<Dot> dots = new ArrayList<Dot>();

    private int m_cellWidth;
    private int m_cellHeight;

    private Rect m_rect = new Rect();
    private Paint m_paint = new Paint();

    private RectF m_circle = new RectF();
    private Paint m_paintCircle = new Paint();

    private Path m_path = new Path();
    private Paint m_paintPath = new Paint();

    private int NUM_CELLS;

    private int gameScore;
    private int movesLeft = 30;

    public Board(Context context, AttributeSet attrs) {
        super(context, attrs);

        getPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        NUM_CELLS = Integer.parseInt(getPrefs.getString("gridSize", "6"));

        m_paint.setColor(Color.WHITE);
        m_paint.setStyle(Paint.Style.STROKE);
        m_paint.setStrokeWidth(2);
        m_paint.setAntiAlias(true);

        m_paintPath.setColor(Color.BLACK);
        m_paintPath.setStrokeWidth(10.0f);
        m_paintPath.setStrokeJoin(Paint.Join.ROUND);
        m_paintPath.setStrokeCap(Paint.Cap.ROUND);
        m_paintPath.setStyle(Paint.Style.STROKE);
        m_paintPath.setAntiAlias(true);
    }

    public void createDots() {
        for(int row = 0; row < NUM_CELLS; ++row) {
            for(int col = 0; col < NUM_CELLS; ++col) {
                int x = col * m_cellWidth;
                int y = row * m_cellHeight;
                Dot d = new Dot(x,y);
                d.m_circle.set(x, y, x + m_cellWidth, y + m_cellHeight);
                d.m_circle.offset(getPaddingLeft(), getPaddingTop());
                d.m_circle.inset(m_cellWidth * 0.15f, m_cellHeight * 0.15f);
                dots.add(d);
            }
        }
    }

    @Override
    protected void onMeasure( int widthMeasureSpec, int heightMeasureSpec ) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width  = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
        int height = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
        int size = Math.min(width, height);
        setMeasuredDimension(size + getPaddingLeft() + getPaddingRight(),
                size + getPaddingTop() + getPaddingBottom());
    }

    @Override
    protected void onSizeChanged( int xNew, int yNew, int xOld, int yOld ) {
        int   boardWidth = (xNew - getPaddingLeft() - getPaddingRight());
        int   boardHeight = (yNew - getPaddingTop() - getPaddingBottom());
        m_cellWidth = boardWidth / NUM_CELLS;
        m_cellHeight = boardHeight / NUM_CELLS;
        m_circle.set(0, 0, m_cellWidth, m_cellHeight);
        m_circle.inset(m_cellWidth * 0.1f, m_cellHeight * 0.1f);
        m_circle.offset(getPaddingLeft(), getPaddingTop());
    }

    @Override
    protected void onDraw(Canvas canvas) {

        if(dots.isEmpty()) {
            createDots();
        }

        int dotNum = 0;
        for ( int row = 0; row < NUM_CELLS; ++row ) {
            for ( int col = 0; col < NUM_CELLS; ++col ) {
                int x = col * m_cellWidth;
                int y = row * m_cellHeight;
                canvas.drawOval( dots.get(dotNum).m_circle, dots.get(dotNum).m_paintCircle);
                dotNum++;
            }
        }

        if ( !m_cellPath.isEmpty() ) {
            m_path.reset();
            Point point = m_cellPath.get(0);
            m_path.moveTo( colToX(point.x) + m_cellWidth/2, rowToY(point.y) + m_cellHeight/2 );
            for ( int i=1; i<m_cellPath.size(); ++i ) {
                point = m_cellPath.get(i);
                m_path.lineTo( colToX(point.x) + m_cellWidth/2, rowToY(point.y) + m_cellHeight/2 );
            }
            canvas.drawPath( m_path, m_paintPath);
        }
    }

    boolean m_moving = false;

    private int xToCol( int x ) {
        int xCol = (x - getPaddingLeft()) / m_cellWidth;
        if(xCol < 0) {
            return 0;
        } else if(xCol > NUM_CELLS - 1) {
            return NUM_CELLS - 1;
        }
        return xCol;
    }
    private int yToRow( int y ) {
        int yRow = (y - getPaddingLeft()) / m_cellWidth;
        if(yRow < 0) {
            yRow = 0;
        } else if(yRow > NUM_CELLS - 1) {
            yRow = NUM_CELLS - 1;
        }
        return yRow;
    }
    private int colToX( int col ) {
        return  col * m_cellWidth + getPaddingLeft();
    }
    private int rowToY( int row ) {
        return  row * m_cellHeight + getPaddingTop();
    }

    private List<Point> m_cellPath = new ArrayList<Point>();

    @Override
    public boolean onTouchEvent( MotionEvent event ) {

        int x = (int) event.getX();
        int y = (int) event.getY();
        int col = xToCol(x);
        int row = yToRow(y);

        int xMax = getPaddingLeft() + m_cellWidth * NUM_CELLS;
        int yMax = getPaddingTop() + m_cellHeight * NUM_CELLS;
        x = Math.max(getPaddingLeft(), Math.min(x, (int) (xMax - m_circle.width())));
        y = Math.max(getPaddingTop(), Math.min(y, (int) (yMax - m_circle.height())));

        Dot curr = getDot(col, row);
        Point currPoint = new Point(col, row);


        if ( event.getAction() == MotionEvent.ACTION_DOWN ) {
            if( m_cellPath.isEmpty()) {
                m_moving = true;
                m_cellPath.add( new Point(xToCol(x), yToRow(y)) );
                m_paintPath.setColor(curr.myColor);
                invalidate();
            }
        }
        else if ( event.getAction() == MotionEvent.ACTION_MOVE ) {
            if ( m_moving ) {
                if ( !m_cellPath.isEmpty( ) ) {
                    if (!m_cellPath.contains(currPoint)) {
                        Point last = m_cellPath.get(m_cellPath.size() - 1);
                        Dot lastDot = getDot(last.x, last.y);
                        // Color check
                        if(curr.myColor == lastDot.myColor)
                        {
                            if(nextDot(last, currPoint)) {
                                m_cellPath.add(new Point(col, row));
                            }
                        }
                    } else {
                        if(m_cellPath.size() >= 2) {
                            if(m_cellPath.get(m_cellPath.size()-2).x == currPoint.x) {
                                if (m_cellPath.get(m_cellPath.size() - 2).y == currPoint.y) {
                                    m_cellPath.remove(m_cellPath.size() - 1);
                                }
                            }
                        }
                    }
                }
                invalidate();
            }

        }
        else if ( event.getAction() == MotionEvent.ACTION_UP ) {
            ArrayList<Integer> indexArr = new ArrayList<Integer>();
            int index = 0;

            for(Point p : m_cellPath) {
                index = (NUM_CELLS * p.y) + p.x;
                indexArr.add(index);
            }
            Collections.sort(indexArr);
            if(indexArr.size() > 1) {
                // Plays sound and vibrates when user is done touching.
                BoardActivity.playSound();
                BoardActivity.vibrate();
                fixBoard(indexArr);
                int pathSize = m_cellPath.size();
                updateGameScore(pathSize);
                movesLeft();
            }
            invalidate();

            if(movesLeft == 0) {
                movesLeftHandler.finalGameScore(gameScore);
            }

            m_moving = false;
            m_cellPath.clear();
            invalidate();
        }
        return true;
    }

    private void updateGameScore(int pathSize) {
        TextView scoreText = (TextView) ((Activity)getContext()).findViewById(R.id.scoreText);
        gameScore += pathSize;
        scoreText.setText("Score: " + gameScore);
    }

    private void movesLeft() {
        TextView movesText = (TextView) ((Activity)getContext()).findViewById(R.id.movesLeft);
        movesLeft--;
        movesText.setText("Moves left: " + movesLeft);
    }

    public void fixBoard(ArrayList<Integer> indexArr) {
        for(Integer i : indexArr) {
            for(int mja = i; mja >= i%NUM_CELLS; ) {
                if(mja - NUM_CELLS < 0) {
                    dots.get(mja).changeColor(Dot.newRandomColor());
                } else {
                    dots.get(mja).changeColor(dots.get(mja - NUM_CELLS).myColor);
                }
                mja -= NUM_CELLS;
            }
        }
        invalidate();
    }

    public boolean nextDot(Point p1, Point p2) {
        int xAbs = Math.abs(p1.x - p2.x);
        int yAbs = Math.abs(p1.y - p2.y);

        if((xAbs + yAbs) == 1) {
            return true;
        }
        return false;
    }

    public Dot getDot(int col, int row) {
        return dots.get(((NUM_CELLS * row) + col));
    }

    ValueAnimator animator = new ValueAnimator();

    public void setMovesLeftEventHandler( OnMovesLeftEventHandler handler ) {
        movesLeftHandler = handler;
    }
}